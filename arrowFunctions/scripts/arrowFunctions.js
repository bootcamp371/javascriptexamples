// function greet() {
//     console.log("Hi!");
// }


let greetMe = () => console.log("Hi");
console.log(greetMe());


// function square(x) {
//     return x * x;
// }

let squarert = (x) => (x * x);

console.log("Square Root of 3 is " + squarert(3));

// function isPositive(x) {
//     return x >= 0;
// }

let isPositive = (x) => (x >= 0);
console.log("5 is greater than 0: " + isPositive(5));
console.log("-2 is greater than 0: " + isPositive(-2));

// function subtract(x, y) {
//     return x - y;
// }

let mySubtract = (a, b) => {return a - b};

console.log("7 - 4 = " + mySubtract(7, 4));

// function biggestOfTwo(x, y) {
//     if (x > y) {
//         return x;
//     } else if (x < y) {
//         return y;
//     }
// }

let whichIsBigger = (x, y) => {
    if (x > y) {
        return x;
    } else if (x < y) {
        return y;
    } else {
        return undefined;
    }
};

console.log("Which is bigger 10 or 15: " + whichIsBigger(10, 15));

// function findBiggest(arr) {
//     let biggest = arr[0];
//     for (let i = 1; i < arr.length; i++) {
//         if (biggest < arr[1]) {
//             biggest = arr[1];
//         }
//     }
//     return biggest;
// }

let findMyBiggest = (myArray) => {
    let biggest = myArray[0];
    for (let i = 1; i < myArray.length; i++) {
        if (biggest < myArray[i]) {
            biggest = myArray[i];
        }
    }
    return biggest;
}

myArray = [2, 4, 17, 6, 9, 10];
console.log("The Biggest number from this list is [2, 4, 17, 6, 9, 10] " + findMyBiggest(myArray));

