"use strict";

window.onload = init;

function init() {

    const convertButton = document.getElementById("convertBtn");
    convertButton.onclick = onConvertBtnClicked;

}

function onConvertBtnClicked() {
    
    const farenheitValue = document.getElementById("FarenheitInput").value;
    let convertedtoC = convertFtoC(farenheitValue);
    const celsiusValue = document.getElementById("celsiusAnswer");
    celsiusValue.value = convertedtoC;

    if (convertedtoC <= 10){
        celsiusValue.style.color = "blue";
    }
    else if (convertedtoC > 10 && convertedtoC <= 23) {
        celsiusValue.style.color = "green";
    }
    else{
        celsiusValue.style.color = "red";
    }
}

function convertFtoC(farenheitValue) {
    let converttoC = 0;
    converttoC = (farenheitValue - 32) * .5556;
    return converttoC;
}
