let pets = [
    {
        name: "Rubby",
        type: "Dog",
        breed: "Corgi",
        bestTrick: "Tug of war",
        image: "images/rubby.jpg"
    }, {
        name: "Howdy",
        type: "Dog",
        breed: "Mixed Breed",
        bestTrick: "Go find it!",
        image: "images/howdy.jpg"
    }, {
        name: "KitKit",
        type: "Cat",
        breed: "American Shorthair",
        bestTrick: "Commanding his owner to feed him",
        image: "images/kitkit.jpg"
    }, {
        name: "Lil' Miss",
        type: "Cat",
        breed: "Tabby",
        bestTrick: "Looking aloof",
        image: "images/lilmiss.jpg"
    }, {
        name: "Happy",
        type: "Dog",
        breed: "Golden Retriever",
        bestTrick: "Refusing to retrieve!",
        image: "images/happy.jpg"
    }, {
        name: "Piper",
        type: "Dog",
        breed: "Beagle",
        bestTrick: "Find it!  Dropped food edition!",
        image: "images/piper.jpg"
    }, {
        name: "Spooky",
        type: "Cat",
        breed: "Mixed",
        bestTrick: "Gymnastics!",
        image: "images/spooky.jpg"
    }
];

window.onload = function () {

    loadCards();
}

function loadCards() {
    const mainDiv = document.getElementById("mainDiv");
    for (let i = 0; i < pets.length; i++) {
        //add Div Class = Card
        let div = document.createElement("div");
        div.className = "card col-md-4 col-sm-6 col-lg-3";
        mainDiv.appendChild(div);

        //add title class-card title
        let divTitle = document.createElement("h5");
        divTitle.className = "card-title";
        divTitle.innerHTML = pets[i].name;
        div.appendChild(divTitle)

        // picture
        let newImage = document.createElement("img");
        newImage.src =  pets[i].image;
        newImage.className = "";
        div.appendChild(newImage);

        //add test stuff
        let textStuff = document.createElement("p")
        textStuff.innerHTML = "Breed: " +  pets[i].breed;
        div.appendChild(textStuff);

        //add test stuff
        let textTrick = document.createElement("p")
        textTrick.innerHTML = "Best Trick: " + pets[i].bestTrick;
        div.appendChild(textTrick);
        
    }
}

