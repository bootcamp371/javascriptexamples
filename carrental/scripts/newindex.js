"use strict";

window.onload = init;

function init() {

    const estimateitButton = document.getElementById("estimateitBtn");
    estimateitButton.onclick = onestimateitButtonClicked;

}

function onestimateitButtonClicked() {

    const numDays = document.getElementById("numDays");
    let totalOptionsFinal = calculateOptions();
    const selectedOption = document.querySelector("input[name='ageCheck']:checked");
    let under25Mult = 0;
    if (selectedOption.value == "under") {
        under25Mult = .3;
    }

    const carCost = document.getElementById("carCost");
    const optionCost = document.getElementById("optionCost");
    const under25Cost = document.getElementById("under25Cost");
    const totalDue = document.getElementById("totalDue");

    let wCarCost = 29.99 * numDays.value;
    carCost.value = wCarCost.toFixed(2);

    let wOptionCost = totalOptionsFinal * numDays.value;
    optionCost.value = wOptionCost.toFixed(2);

    let wUnder25Cost = ((totalOptionsFinal * numDays.value) +
        (29.99 * numDays.value)) * under25Mult;
    under25Cost.value = wUnder25Cost.toFixed(2);
    alert("got this far");

    alert(carCost.value);
    let wgrandTotal = Number(carCost.value) +
        Number(optionCost.value) +
        Number(under25Cost.value);
    totalDue.value = wgrandTotal.toFixed(2);
    return false;

}

function calculateOptions() {
    const tollTag = document.getElementById("tollTag");
    const gps = document.getElementById("gps");
    const roadside = document.getElementById("roadside");
    let totalOptions = 0;
    if (document.getElementById("tollTag").checked == true) {
        totalOptions = totalOptions + 3.95;
    }
    if (document.getElementById("gps").checked == true) {
        totalOptions = totalOptions + 2.95;
    }
    if (document.getElementById("roadside").checked == true) {
        totalOptions = totalOptions + 2.95;
    }
    return totalOptions;
}
