let imageFiles = [
    { name: "images/bear.jpg", description: "Big Bear" },
    { name: "images/Lilly.jpg", description: "Pretty Lilly" },
    { name: "images/money.jpg", description: "Laundered Money" },
    { name: "images/smiley.jpg", description: "Don't Worry Be Happy" },
    { name: "images/weirdfish.jpg", description: "Weird Fish" }
];

window.onload = function () {

    loadImages();
    
    document.getElementById("addImage").onclick = onAddBtnClicked;
    document.getElementById("clearImages").onclick = onClearBtnClicked;
    

};

function onClearBtnClicked() {
    // longer way
    // let removeImages = document.getElementsById("divImages");
    // removeImages.innerHTML = ""

    document.getElementsById("divImages").innerHTML = "";

}

function onAddBtnClicked() {
    // const selectedImage = document.getElementById("imageList");

    // let newImage = document.createElement("img");
    // newImage.src = selectedImage.value;
    // newImage.alt = selectedImage.text;

    // let addDiv = document.getElementById("divImages");
    // addDiv.appendChild(newImage);
    // return false;

    //This is how you need to do it to get the alt info
    const selectedImage = document.getElementById("imageList").selectedIndex;

    let newImage = document.createElement("img");
    newImage.src = imageFiles[selectedImage].name;
    newImage.alt = imageFiles[selectedImage].description;
    let addDiv = document.getElementById("divImages");
    addDiv.appendChild(newImage);
    return false;

}

function loadImages() {

    const imageList = document.getElementById("imageList");
    imageList.appendChild(new Option("Select an Image", ""));
    for (let i = 0; i < imageFiles.length; i++) {
        imageList.appendChild(new Option(imageFiles[i].description, imageFiles[i].name));
    }
}