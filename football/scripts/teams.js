"use strict";

let teams = [
    { code: "DAL", name: "Dallas Cowboys", plays: "Arlington, TX" },
    { code: "DEN", name: "Denver Broncos", plays: "Denver, CO" },
    { code: "HOU", name: "Houston Texans", plays: "Houston, TX" },
    { code: "KAN", name: "Kansas City Chiefs", plays: "Kansas City, MO" },
];

window.onload = function () {

    document.getElementById("addTeam").onclick = onaddTeamClicked;
    loadTeams();
    document.getElementById("teamInfo").onclick = onBtnClicked;
    document.getElementById("teamsList").onchange = onSelectChange;

};

function onSelectChange() {
    const p = document.getElementById("teamInfoText");
    const teamsList = document.getElementById("teamsList");
    const img = document.getElementById("NFL");

    if (teamsList.value == "") {
        document.getElementById("NFL").src = "images/NFL.PNG";
        p.innerHTML = " ";
    } else {

        document.getElementById("NFL").src = "images/" + teamsList.value + ".PNG";

    }
}
function onaddTeamClicked() {

    const teamName = document.getElementById("teamName").value;
    const teamAbbr = document.getElementById("teamCity").value;
    const playsWhere = document.getElementById("playsWhere").value;
    const teamsList = document.getElementById("teamsList");

    teams.push({ code: teamAbbr, name: teamName, plays: playsWhere });

    teamsList.appendChild(new Option(teamName, teamAbbr));

    return false;

}

function loadTeams() {

    const teamsList = document.getElementById("teamsList");
    teamsList.appendChild(new Option("Select a team", ""));


    for (let i = 0; i < teams.length; i++) {
        teamsList.appendChild(new Option(teams[i].name, teams[i].code));
    }
}

function onBtnClicked() {

    const p = document.getElementById("teamInfoText");
    const teamsList = document.getElementById("teamsList");
    for (let i = 0; i < teams.length; i++) {

        if (teams[i].code == teamsList.value) {

            p.innerHTML = "You selected the " + teams[i].name + "(" + teams[i].code +
                ") who plays in " + teams[i].plays;
        }

    }
    if (teamsList.value == "") {

        p.innerHTML = " ";
    }
    return false;
}
