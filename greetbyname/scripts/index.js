"use strict";

window.onload = init;

function init() {

    const greetButton = document.getElementById
        ("greetBtn");
    greetButton.onclick = onGreetUserBtnClicked;

}

function onGreetUserBtnClicked() {
    
    const getName = document.getElementById("nameField");
    const sendMessage = document.getElementById("sendMessage");
    let getNameValue = getName.value;
    let message = "Hello " + getNameValue;
    sendMessage.innerHTML = message;
}
