function processCoffeeLeft() {

    const userName = document.getElementById("nameInput").value;
    const hobby = document.getElementById("hobbyInput").value;
    const color = document.getElementById("colorInput").value;
    const maxCoffee = document.getElementById("maxCoffeeInput").value;
    const coffeeConsumed = document.getElementById("coffeeConsumedInput").value;
    const messageDiv = document.getElementById("sendMessage");


    let coffeeLeft = maxCoffee - coffeeConsumed;
    let message = "Hi " + userName +
        "You like to play " + hobby +
        "Your Favorite Color is " + color +
        "You have " + coffeeLeft + " cups of coffee left to drink";
    //alert(message);

    console.log();


    messageDiv.innerHTML = message;
    messageDiv.style.color = color;


}