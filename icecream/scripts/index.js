"use strict";

window.onload = function () {
    const cone = document.getElementById("cone");
    const cup = document.getElementById("cup");
    const scoops = document.getElementById("scoops");

    
    document.getElementById("orderBtn").onclick = onOrderBtnClicked;
    cone.onclick = onConeClick;
    cup.onclick = onCupClick;

    scoops.onchange = onscoops;

}

function onscoops (){
    return;
}

function onCupClick() {
    document.getElementById("hidetoppings").style.visibility = "visible";

}

function onConeClick() {
    document.getElementById("hidetoppings").style.visibility = "hidden";

}

function onOrderBtnClicked() {

    const scoops = document.getElementById("numScoops");
    alert("yes I am");
    if (scoops.value < 1 || scoops.value > 4) {
        // alert("You must have between 1 and 4 scoops");
        document.getElementById("scoopcheck").style.color = "red";
        document.getElementById("scoopcheck").innerHTML = "You must select between 1 and 4 scoops"; 
        
        return false;
    }

    let baseAmt = 2.25 + ((scoops.value - 1) * 1.25);

    alert("cup check" + cup.checked);
    if (cup.checked) {
        if (sprinkles.checked) {
            baseAmt = Number(baseAmt) + .5;
        }
        if (whippedcream.checked) {
            baseAmt = Number(baseAmt) + .25;
        }
        if (hotfudge.checked) {
            baseAmt = Number(baseAmt) + 1.25;
        }
        if (cherry.checked) {
            baseAmt = Number(baseAmt) + .25;
        }

    }
    alert(baseAmt);
    const basePrice = document.getElementById("basePrice");
    const taxDue = document.getElementById("taxDue");
    const totalDue = document.getElementById("totalDue");

    basePrice.value = baseAmt;
    taxDue.value = baseAmt * .06;
    totalDue.value = baseAmt + baseAmt * .06;
    return false;
}