// page 228

let courses = [
    {
        CourseId: "PROG100",
        Title: "Introduction to HTML/CSS/Git",
        Location: "Classroom 7",
        StartDate: "09/08/22",
        Fee: "100.00",
    },
    {
        CourseId: "PROG200",
        Title: "Introduction to JavaScript",
        Location: "Classroom 9",
        StartDate: "11/22/22",
        Fee: "350.00",
    },
    {
        CourseId: "PROG300",
        Title: "Introduction to Java",
        Location: "Classroom 1",
        StartDate: "01/09/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROG400",
        Title: "Introduction to SQL and Databases",
        Location: "Classroom 7",
        StartDate: "03/16/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROJ500",
        Title: "Introduction to Angular",
        Location: "Classroom 1",
        StartDate: "04/25/23",
        Fee: "50.00",
    }
];


// Course Start Date
let courseStartDate = getCourseStartDate(courses, "PROG200", "CourseId");

for (let i = 0; i < courseStartDate.length; i++) {

    console.log(courseStartDate[i].CourseId + " starts on " + courseStartDate[i].StartDate);
}


function getCourseStartDate(courses, inputValue, key) {
    let matching = [];

    for (let i = 0; i < courses.length; i++) {

        if (courses[i][key] == inputValue) {
            matching.push(courses[i]);
        }

    }

    return matching;
}

// Course Title
let courseTitle = getCourseTitle(courses, "PROJ500", "CourseId");

for (let i = 0; i < courseTitle.length; i++) {
    console.log(courseTitle[i].CourseId + " Title: " + courseTitle[i].Title);
}
function getCourseTitle(courses, inputValue, key) {
    let matching = [];

    for (let i = 0; i < courses.length; i++) {

        if (courses[i][key] == inputValue) {
            matching.push(courses[i]);
        }
    }
    return matching;
}

// Less than $50

let course50 = getCourse50(courses, 50);

for (let i = 0; i < course50.length; i++) {
    console.log(course50[i].Title + " Fee: " + course50[i].Fee);
}
function getCourse50(courses, inputValue) {
    let matching = [];

    for (let i = 0; i < courses.length; i++) {

        if (courses[i].Fee <= inputValue) {
            matching.push(courses[i]);
        }
    }
    return matching;
}

let courseLocation = getLocation(courses, "Classroom 1");

for (let i = 0; i < courseLocation.length; i++) {
    console.log(courseLocation[i].Title + " Location: " + courseLocation[i].Location);
}
function getLocation(courses, inputValue) {
    let matching = [];

    for (let i = 0; i < courses.length; i++) {

        if (courses[i].Location == inputValue) {
            matching.push(courses[i]);
        }
    }
    return matching;
}