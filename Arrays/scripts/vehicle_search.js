let vehicles = [
    {
        color: "Silver",
        type: "Minivan",
        registrationState: "CA",
        licenseNo: "ABC-101",
        registrationExpires: new Date("3-10-2022"),
        capacity: 7
    },
    {
        color: "Red",
        type: "Pickup Truck",
        registrationState: "TX",
        licenseNo: "A1D-2NC",
        registrationExpires: new Date("8-31-2023"),
        capacity: 3
    },
    {
        color: "White",
        type: "Pickup Truck",
        registrationState: "TX",
        licenseNo: "A22-X00",
        registrationExpires: new Date("9-31-2023"),
        capacity: 6
    },
    {
        color: "Red",
        type: "Car",
        registrationState: "CA",
        licenseNo: "ABC-222",
        registrationExpires: new Date("12-10-2022"),
        capacity: 5
    },
    {
        color: "Black",
        type: "SUV",
        registrationState: "CA",
        licenseNo: "EEE-222",
        registrationExpires: new Date("12-10-2021"),
        capacity: 7
    },
    {
        color: "Red",
        type: "SUV",
        registrationState: "TX",
        licenseNo: "ZZ2-101",
        registrationExpires: new Date("12-30-2022"),
        capacity: 5
    },
    {
        color: "White",
        type: "Pickup Truck",
        registrationState: "TX",
        licenseNo: "CAC-7YT",
        registrationExpires: new Date("1-31-2023"),
        capacity: 5
    },
    {
        color: "White",
        type: "Pickup Truck",
        registrationState: "CA",
        licenseNo: "123-ABC",
        registrationExpires: new Date("3-31-2023"),
        capacity: 5
    }
];

// red ones
console.log("---------------");
console.log("Starts with Red");
console.log("---------------");
let redCars = findRed(vehicles, "Red");

for (let i = 0; i < redCars.length; i++) {

    console.log(redCars[i].licenseNo, redCars[i].type, redCars[i].color);
}

function findRed(vehicles, inputValue) {
    let matching = [];
    for (let i = 0; i < vehicles.length; i++) {
        if (vehicles[i].color == inputValue) {
            matching.push(vehicles[i]);
        }
    }
    return matching;
}

console.log("---------------");
console.log("Expired");
console.log("---------------");

let expiredCars = findExpired(vehicles, "Red");

for (let i = 0; i < expiredCars.length; i++) {

    console.log(expiredCars[i].licenseNo, expiredCars[i].type, expiredCars[i].registrationExpires);
}

function findExpired(vehicles, inputValue) {
    let matching = [];
    let d1 = new Date();
    for (let i = 0; i < vehicles.length; i++) {

        if (vehicles[i].registrationExpires < d1) {
            matching.push(vehicles[i]);
        }
    }
    return matching;
}

console.log("---------------");
console.log("6 people");
console.log("---------------");

let sixCars = find6(vehicles, 6);

for (let i = 0; i < sixCars.length; i++) {

    console.log(sixCars[i].licenseNo, sixCars[i].type, sixCars[i].capacity);
}

function find6(vehicles, inputValue) {
    let matching = [];

    for (let i = 0; i < vehicles.length; i++) {

        if (vehicles[i].capacity >= inputValue) {
            matching.push(vehicles[i]);
        }
    }
    return matching;
}

//ends 222
console.log("---------------");
console.log("end 222");
console.log("---------------");

let twoCars = find222(vehicles, 222);

for (let i = 0; i < twoCars.length; i++) {

    console.log(twoCars[i].licenseNo, twoCars[i].type, twoCars[i].capacity);
}

function find222(vehicles, inputValue) {
    let matching = [];

    for (let i = 0; i < vehicles.length; i++) {

        if (vehicles[i].licenseNo.endsWith(inputValue)) {
            matching.push(vehicles[i]);
        }
    }
    return matching;
}
