let academyMembers = [
    {
        memID: 101,
        name: "Bob Brown",
        films: ["Bob I", "Bob II", "Bob III", "Bob IV"]
    },
    {
        memID: 142,
        name: "Sallie Smith",
        films: ["A Good Day", "A Better Day"]
    },
    {
        memID: 187,
        name: "Fred Flanders",
        films: ["Who is Fred?", "Where is Fred?",
            "What is Fred?", "Why Fred?"]
    },
    {
        memID: 203,
        name: "Bobbie Boots",
        films: ["Walking Boots", "Hiking Boots",
            "Cowboy Boots"]
    },
];

// Actor 187
console.log("---------------");
console.log("Actor 187");
console.log("---------------");

let actor187 = getActor187(academyMembers, 187);

for (let i = 0; i < actor187.length; i++) {

    console.log(actor187[i].name, actor187[i].memID);
}

function getActor187(academyMembers, inputValue) {
    let matching = [];

    for (let i = 0; i < academyMembers.length; i++) {

        if (academyMembers[i].memID == inputValue) {
            matching.push(academyMembers[i]);

        }
    }
    return matching;
}

// at least 3 films
console.log("---------------");
console.log("3 Films");
console.log("---------------");
let actor3 = getActor3(academyMembers, 3);

for (let i = 0; i < actor3.length; i++) {

    console.log(actor3[i].name, actor3[i].films);
}

function getActor3(academyMembers, inputValue) {
    let matching = [];

    for (let i = 0; i < academyMembers.length; i++) {

        if (academyMembers[i].films.length >= inputValue) {
            matching.push(academyMembers[i]);
        }
    }
    return matching;
}

// Starts with Bob
console.log("---------------");
console.log("Starts with Bob");
console.log("---------------");
let actorBob = getActorBob(academyMembers, "Bob");

for (let i = 0; i < actorBob.length; i++) {

    console.log(actorBob[i].name);
}

function getActorBob(academyMembers, inputValue) {
    let matching = [];

    for (let i = 0; i < academyMembers.length; i++) {

        if (academyMembers[i].name.startsWith(inputValue)) {
            matching.push(academyMembers[i]);
        }
    }
    return matching;
}

