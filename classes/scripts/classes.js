class Employee {
    constructor(id, firstName, lastName, jobTitle, payRate) {
        this.employeeId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }

    getFullName() {
        return this.firstName + " " + this.lastName;
    }

    promote(newTitle, newPayRate) {

        this.jobTitle = newTitle;
        this.payRate = newPayRate;

    }

    getIntro() {
        return "Hi! I'm " + this.firstName + " " + this.lastName +
            " and I am a " + this.jobTitle;
    }
}

let employee1 = new Employee(
    1, "Ian", "Auston", "Graphic Artist", 42.50);
console.log(`Employee ${employee1.getFullName()} created`);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`);

employee1.promote("Sr. Graphic Artist", 46.75);
console.log(`Job title is ${employee1.jobTitle}`);
console.log(`Pay rate is $${employee1.payRate}`);

let intro = employee1.getIntro();
console.log(intro);

let employee2 = new Employee(
    1, "Mark", "Galasso", "Space Cowboy", 250.50);

console.log(`Employee ${employee2.getFullName()} created`);
console.log(`Job title is ${employee2.jobTitle}`);
console.log(`Pay rate is $${employee2.payRate}`);

employee2.promote("Superman", 275.75);
console.log(`Job title is ${employee2.jobTitle}`);
console.log(`Pay rate is $${employee2.payRate}`);

let intro2 = employee2.getIntro();
console.log(intro2);