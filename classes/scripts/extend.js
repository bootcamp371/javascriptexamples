class Person {

    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName
    };
    getFullName() {
        return this.firstName + " " + this.lastName;
    }
}

class Employee extends Person {
    constructor(id, jobTitle, payRate, firstName, lastName) {
        super(firstName, lastName);
        this.id = id;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }

    getGrossPay(hoursWorked) {
        return this.payRate * hoursWorked;
    }
}

class Manager extends Employee {
    constructor(numEmployees, id, jobTitle, payRate, firstName, lastName) {
        super(id, jobTitle, payRate, firstName, lastName);
        this.numEmployees = numEmployees;
        
    }

}

let person1 = new Person("John", "Smith");
console.log(`Employee ${person1.getFullName()} created`);

let employee1 = new Employee("001", "Programmer", 40.25, "Mark", "Galasso");

console.log(`Employee ${employee1.getFullName()} created`);
console.log(`Employee made ${employee1.getGrossPay(40)} last week`);

let employee2 = new Employee("002", "Engineer", 45.25, "Joe", "Mamma");

console.log(`Employee ${employee2.getFullName()} created`);
console.log(`Employee made ${employee2.getGrossPay(40)} last week`);

let  manager1 = new Manager(2, "003", "Manager", 75.00, "Sparky", "Anderson");
console.log(`Manager ${manager1.getFullName()} created`);
console.log(`Manager made ${manager1.getGrossPay(40)} last week`);
console.log(`${manager1.getFullName()} has ${manager1.numEmployees} employees`);
