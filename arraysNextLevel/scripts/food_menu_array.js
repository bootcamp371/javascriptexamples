let menu = [
    [
        { item: "Sausage and Egg Biscuit", price: 3.69 },
        { item: "Bacon and Egg Biscuit", price: 3.49 },
        { item: "Ham and Egg Biscuit", price: 3.29 }
    ],
    [
        { item: "Ham and Cheese", price: 5.69 },
        { item: "Gyro and Fries", price: 5.49 },
        { item: "Big Mac and Fries", price: 5.29 },
        { item: "Whopper and Fries", price: 5.79 },
    ],
    [
        { item: "Steak and Potato", price: 7.69, ingrediants: ["steak" , "potato", "butter", "chives"] },
        { item: "Chicken and Rice", price: 7.49 },
        { item: "Pasta and Meatballs", price: 7.19 },
        { item: "Meat on a plate", price: 7.99 },
        { item: "Ham and Mashed Potatoes", price: 7.29 }
    ]
];

let meal = 2;
let plateNum = 3;
let type = ["Breakfast Menu", "Lunch Menu", "Dinner Menu"]
getMeal(menu, meal, type, plateNum);

function listThem(dish){
    console.log(dish.item + " Price: "  + dish.price); 
}

function getMeal (menu, mealNum, typeofmenu, plateNum){
    
    console.log(typeofmenu[mealNum]);
    //arrow way
    menu[mealNum].forEach(dish => console.log(dish.item + " Price: "  + dish.price)); 

    //long boring way
    // menu[mealNum].forEach(listThem); 

    // Have fun with this later
    //menu[mealNum][0].ingrediants.forEach(stuff => console.log(stuff)); 

    //console.log(menu[mealNum][plateNum]);

}