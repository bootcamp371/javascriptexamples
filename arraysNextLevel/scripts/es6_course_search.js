let courses = [
    {
        CourseId: "PROG100",
        Title: "Introduction to HTML/CSS/Git",
        Location: "Classroom 7",
        StartDate: "09/08/22",
        Fee: "100.00",
    },
    {
        CourseId: "PROG200",
        Title: "Introduction to JavaScript",
        Location: "Classroom 9",
        StartDate: "11/22/22",
        Fee: "350.00",
    },
    {
        CourseId: "PROG300",
        Title: "Introduction to Java",
        Location: "Classroom 1",
        StartDate: "01/09/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROG400",
        Title: "Introduction to SQL and Databases",
        Location: "Classroom 7",
        StartDate: "03/16/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROJ500",
        Title: "Introduction to Angular",
        Location: "Classroom 1",
        StartDate: "04/25/23",
        Fee: "50.00",
    }
];

// find when PROG 200 starts
console.log("---------------");
console.log("When does PROG200 Start");
console.log("---------------");
function findCoursePROG200(items) {

    if (items.CourseId == "PROG200") {
        return true;
        
    }
    return false;
}

let myPROG200 = courses.find(findCoursePROG200);
console.log(myPROG200.Title + " starts " + myPROG200.StartDate);


//What is the title of the PROJ500 course
console.log("---------------");
console.log("What is the Title of PROJ500");
console.log("---------------");

function findCoursePROG500(items) {

    if (items.CourseId == "PROJ500") {
        return true;
        
    }
    return false;
}

let myPROJ500 = courses.find(findCoursePROG500);
console.log(myPROJ500.Title + " is the title of " + myPROJ500.CourseId);

//What are the titles of the courses that cost $50 or less?
console.log("---------------");
console.log("What costs $50 or less");
console.log("---------------");

function findLess50(items){

    if (items.Fee <= 50) {
        return true;
        
    }
    return false;
}

let myLess50 = courses.filter(findLess50);

for (let i = 0; i < myLess50.length; i++){
    console.log(myLess50[i].CourseId + " is " + myLess50[i].Fee);
}

//What classes meet in Classroom 1
console.log("---------------");
console.log("Meet in Classroom 1");
console.log("---------------");


function findMeetin1(items){

    if (items.Location == "Classroom 1") {
        return true;
        
    }
    return false;
}

let myMeetin1 = courses.filter(findMeetin1);

for (let i = 0; i < myMeetin1.length; i++){
    console.log(myMeetin1[i].CourseId + " meets in " + myMeetin1[i].Location);
}

