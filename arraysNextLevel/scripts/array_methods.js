let cart = [
    { item: "Bread", price: 3.29, quantity: 2 },
    { item: "Milk", price: 4.09, quantity: 1 },
    { item: "T-Bone Steak", price: 12.99, quantity: 2 },
    { item: "Baking Potato", price: 1.89, quantity: 6 },
    { item: "Iceberg Lettuce", price: 2.06, quantity: 1 },
    { item: "Ice Cream - Vanilla", price: 6.81, quantity: 1 },
    { item: "Apples", price: 0.66, quantity: 6 }
];


//Get item list into an array and display..oh yeah, and sort it

console.log("---------------");
console.log("Get item list into an array and display..oh yeah, and sort it");
console.log("---------------");
function getItemName(arrayGeneric){
    
    return arrayGeneric.item;
}
function displayItemList(arrayGeneric){
    console.log(arrayGeneric);
}

let itemList = cart.map(getItemName);
let sortedList = itemList.sort();
itemList.forEach(displayItemList);

// Use Reduce to get the total don't forget about qty
console.log("---------------");
console.log("Use Reduce to get the total don't forget about qty");
console.log("---------------");

function grandTotalFunction(currentTotal, inputArray){
    return currentTotal + (inputArray.price * inputArray.quantity);
}

let grandTotal = cart.reduce(grandTotalFunction, 0)
console.log(grandTotal);

