"use strict";

window.onload = init;

function init() {

    const addButton = document.getElementById("addBtn");
    const subButton = document.getElementById("subBtn");
    const multButton = document.getElementById("multBtn");
    const divButton = document.getElementById("divBtn");
    addButton.onclick = addBtnClicked;
    subButton.onclick = subBtnClicked;
    multButton.onclick = multBtnClicked;
    divButton.onclick = divBtnClicked;

}

function addBtninput() {

    const num1 = Number(document.getElementById("numberInput1").value);
    const num2 = Number(document.getElementById("numberInput2").value);
    const answerBox = document.getElementById("answerField");
    let theAnswer = num1 + num2;
    answerBox.value = theAnswer;
}

function addBtnClicked() {

    const num1 = Number(document.getElementById("numberInput1").value);
    const num2 = Number(document.getElementById("numberInput2").value);
    const answerBox = document.getElementById("answerField");
    let theAnswer = num1 + num2;
    answerBox.value = theAnswer;
}

function subBtnClicked() {
    const num1 = Number(document.getElementById("numberInput1").value);
    const num2 = Number(document.getElementById("numberInput2").value);
    const answerBox = document.getElementById("answerField");
    let theAnswer = num1 - num2;
    answerBox.value = theAnswer;

}
function multBtnClicked() {
    const num1 = Number(document.getElementById("numberInput1").value);
    const num2 = Number(document.getElementById("numberInput2").value);
    const answerBox = document.getElementById("answerField");
    let theAnswer = num1 * num2;
    answerBox.value = theAnswer;

}
function divBtnClicked() {
    const num1 = Number(document.getElementById("numberInput1").value);
    const num2 = Number(document.getElementById("numberInput2").value);
    const answerBox = document.getElementById("answerField");
    let theAnswer = num1 / num2;
    answerBox.value = theAnswer;
}