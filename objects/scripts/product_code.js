let partCode1 = "XYZ:1234-L";
let part1 = parsePartCode(partCode1);

console.log("Supplier: " + part1.supplierCode +
" Product Number: " + part1.productNumber +
" Size: " + part1.size)

function parsePartCode(part1){
    let colPos = part1.indexOf(":");
    let dashPos = part1.indexOf("-");
    let supplier = part1.substring(0,colPos);
    let productNum = part1.substring(colPos + 1, dashPos);
    let size = part1.substring(dashPos + 1)
    
    
    let productObj = {
        supplierCode: supplier,
        productNumber: productNum,
        size: size
    };
    return productObj;

}


// console.log("Supplier: " + part1.supplierCode +
// " Product Number: " + part1.productNumber +
// " Size: " + part1.size);