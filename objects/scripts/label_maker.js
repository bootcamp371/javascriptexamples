let myInfo = {
    name: "Mark Galasso",
    address: "121 Main Street",
    city: "Detroit",
    state: "MI",
    zip: "48044"
}
printContact(myInfo);

function printContact(myInfo) {

    console.log(myInfo.name);
    console.log(myInfo.address);
    console.log(myInfo.city + ",", myInfo.state, myInfo.zip);

}
